#! /usr/bin/env python

# Copyright (C) 2017 by Elvis Angelaccio <elvis.angelaccio@kde.org>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os
import subprocess

from argparse import ArgumentParser
from configparser import ConfigParser
from enum import Enum
from lib.cmake import BuildJob, CMakeJob, CTestJob
from lib.env import init_env
from lib.run import GdbJob, RunJob
from lib.utils import merge_key_for
from lib.xdg import cache_dir, config_dir
from shutil import rmtree


class SubCommand(Enum):
    build = 1
    run = 2
    debug = 3


class Edk:
    def __init__(self, config, arg_parser):
        self.config = config

        edk, self.run_args = arg_parser.parse_known_args()
        self.project = edk.project
        self.clean_build = 'clean_build' in edk and edk.clean_build

        self.executable = edk.executable if 'executable' in edk else ''
        if not self.executable and self.project in config and 'executable' in config[self.project]:
            self.executable = config[self.project]['executable']

        self.sub_command = SubCommand.build
        if edk.sub_command == 'r' or edk.sub_command == 'run':
            self.sub_command = SubCommand.run
        elif edk.sub_command == 'd' or edk.sub_command == 'debug':
            self.sub_command = SubCommand.debug

    def setup_build_dir(self):
        build_dir = cache_dir() + '/' + self.project

        if self.clean_build and os.path.isdir(build_dir):
            assert 'edk' in build_dir, '%s does not look like an edk build directory, cannot remove it.' % build_dir
            print('Removing old build directory: %s' % build_dir)
            rmtree(build_dir)

        os.makedirs(build_dir, exist_ok=True)
        return build_dir


    def pipeline(self, project_dir):
        if self.sub_command is SubCommand.run or self.sub_command is SubCommand.debug:
            job = RunJob if self.sub_command is SubCommand.run else GdbJob
            executable = self.executable
            # No executable defined. Use the project name as executable name.
            if not executable:
                executable = self.project
            return [job(executable, self.run_args)]

        cmake_args = merge_key_for(self.project, 'cmake-options', self.config)
        make_args = merge_key_for(self.project, 'make-options', self.config)
        ctest_args = merge_key_for(self.project, 'ctest-options', self.config)

        return [
            CMakeJob(project_dir, cmake_args),
            BuildJob(make_args),
            CTestJob(ctest_args)
        ]


def main():
    config = ConfigParser()
    config.read(config_dir() + '/config.ini')

    project = config['global'].get('project') if 'global' in config else ''
    if not project:
        project = os.path.basename(os.getcwd())

    arg_parser = ArgumentParser(prog='edk', description='easily build CMake projects')
    arg_parser.add_argument('-p', '--project', nargs='?', default=project, help='the project edk will work with')
    subparser = arg_parser.add_subparsers(title='valid subcommands', dest='sub_command', metavar='{SUBCOMMAND}')

    build_parser = subparser.add_parser('b', aliases=['build'], help='build/install the project and run the tests if available')
    build_parser.add_argument('-c', '--clean', action='store_true', help='perform a clean build', dest='clean_build')

    run_parser = subparser.add_parser('r', aliases=['run'], help='run the project (with arguments, if any)')
    run_parser.add_argument('-e', '--executable', nargs='?', help='run this executable (with arguments, if any)')
    debug_parser = subparser.add_parser('d', aliases=['debug'], help='run the project with GDB (with arguments, if any)')
    debug_parser.add_argument('-e', '--executable', nargs='?', help='debug this executable with gdb')

    edk = Edk(config, arg_parser)
    init_env(config_dir() + '/env.sh')

    # edk is supposed to be run from the top-level cmake dir
    project_dir = os.getcwd()

    build_dir = edk.setup_build_dir()
    os.chdir(build_dir)

    for job in edk.pipeline(project_dir):
        print('Running: %s' % job)
        if subprocess.call(job.command()) != 0:
            print('Job failed (%s).' % job)
            break

if __name__ == '__main__':
    main()
