# Copyright (C) 2017 by Elvis Angelaccio <elvis.angelaccio@kde.org>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os

from .job import Job
from shutil import which


class BuildJob(Job):
    def __init__(self, options=None):
        super().__init__(options)

    def command(self):
        return ['cmake', '--build', '.', '--target', 'install', '--'] + self.options


class CMakeJob(Job):
    def __init__(self, src_dir, options=None):
        super().__init__(options)
        self.src_dir = src_dir

    def command(self):
        args = self.validate_args()
        if os.environ.get('EDK'):
            args = ['-DCMAKE_INSTALL_PREFIX=%s' % os.environ['EDK']] + self.options
        return ['cmake'] + args + [self.src_dir]

    def validate_args(self):
        args = self.options
        return [a for a in args if not a.startswith('-DCMAKE_INSTALL_PREFIX=') and a != self.src_dir]


class CTestJob(Job):
    def __init__(self, options=None):
        super().__init__(options)

    def command(self):
        # If possible, run the tests in a virtual X server.
        if which('xvfb-run'):
            return ['xvfb-run', '-s', '-screen 0 1024x768x24', 'ctest'] + self.options
        return ['ctest'] + self.options
