# Copyright (C) 2017 by Elvis Angelaccio <elvis.angelaccio@kde.org>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os


def lookup_xdg_dir(xdg_dir):
    dir = os.environ[xdg_dir]
    if dir[-1] != '/':
        dir += '/'
    return dir


def cache_dir():
    if 'XDG_CACHE_HOME' not in os.environ:
        return os.path.expanduser('~') + '/.cache/edk'

    return lookup_xdg_dir('XDG_CONFIG_HOME') + 'edk'


def config_dir():
    if 'XDG_CONFIG_HOME' not in os.environ:
        return os.path.expanduser('~') + '/.config/edk'

    return lookup_xdg_dir('XDG_CONFIG_HOME') + 'edk'


def edk_dir():
    if 'XDG_DATA_HOME' not in os.environ:
        return os.path.expanduser('~') + '/.local/share/edk'

    return lookup_xdg_dir('XDG_DATA_HOME') + 'edk'



