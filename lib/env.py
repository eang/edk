# Copyright (C) 2017 by Elvis Angelaccio <elvis.angelaccio@kde.org>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os
import subprocess

from .xdg import edk_dir


def init_env(script):
    if os.path.isfile(script):
        command = ['bash', '-c', ('source %s && env' % script)]
        process = subprocess.Popen(command, stdout=subprocess.PIPE, universal_newlines=True)

        for line in process.stdout:
            (key, _, value) = line.partition('=')
            os.environ[key] = value.rstrip('\r\n')

    validate_env()


def validate_env():

    edk = edk_dir()

    lib_prefix = '/lib'  # TODO: support for /lib64, /lib/x86_64-linux-gnu, ...

    validate_var('EDK', edk)
    validate_var('CMAKE_PREFIX_PATH', edk)
    validate_var('XDG_DATA_DIRS', edk + '/share')
    validate_var('XDG_CONFIG_DIRS', edk + '/etc/xdg')
    validate_var('QML2_IMPORT_PATH', edk + lib_prefix + '/qml')
    validate_var('QT_PLUGIN_PATH', edk + lib_prefix + '/plugins')
    validate_var('PATH', edk + '/bin')

    # LD_LIBRARY_PATH conflicts with the RPATH
    if 'LD_LIBRARY_PATH' in os.environ:
        del os.environ['LD_LIBRARY_PATH']


def validate_var(var, value):
    if var not in os.environ:
        os.environ[var] = value

    # var already in the env, just prepend our value
    if not os.environ[var].startswith(value):
        os.environ[var] = value + ':' + os.environ[var]
