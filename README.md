# edk: easy development kit

`edk` is a Python script that can be used to easily build and develop CMake projects.

# How easily?

Very easily. If properly installed and configured, building any CMake project is as easy as invoking `edk` from the root of the project.

# Installation

```shell
git clone https://gitlab.com/eang/edk.git
# assuming that $HOME/bin is in your $PATH
ln -s $PWD/edk/edk.py $HOME/bin/edk
# optional, if you want to customize your build settings or environment variables
mkdir ~/.config/edk
cp edk/config.ini.example ~/.config/edk/config.ini
cp edk/env.sh.example ~/config/edk/env.sh
```

(you can replace `~/.config` with `$XDG_CONFIG_HOME`, if you have this variable set)

# How does it work?
`edk` is designed to be simple. It will just build, install and (optionally) "run" your project (as `edk` is primarily designed for applications with a single executable). You don't need to worry about setting up a development environment (besides installing `edk` itself).
`edk` is not a replacement of tools like `kdesrc-build`. It doesn't download source code and it doesn't resolve dependencies (in fact, `edk` assumes that all build-dependencies are already satisfied).

# Usage
## Build a project
From the git root of the project:

    edk

This is a shorthand for `edk b` with no additional arguments.
The script will perform the following operations:

- If `$XDG_CONFIG_HOME/edk/env.sh` exists, it will source whatever environment variables are exported in there.
- Build the CMake-based project found in `$(pwd)`. Build files are written to a subdirectory of `$XDG_CACHE_HOME/edk`.
- Install the project in `$EDK`. This variable defaults to `$XDG_DATA_HOME/edk`, but you can override it in `env.sh`.
- Run the project tests (if available). If `xvfb-run` is installed, tests will be run in a virtual X server.

## Configure the project

`edk` will respect the options defined in the `[project]` section of the config file.
The value of the `project` key is defined in the following order:

1. The value of the `-p` switch, for example `edk -p foo`
2. The value of the `project` key in the `[global]` section of the config file
3. The name of the directory `edk` was run from.

The value of this key is also used as the name of the build directory in `$XDG_CACHE_HOME/edk`.

## Run the project

Once the project is built, you can run it with:

    edk r

This will execute whatever is defined in the `project` key, unless you define an `executable` key in the `[project]` section. You can override both with the `-e` switch:

    edk r -e autotests/footest

will run the `autotests/footest` executable located in your build directory.

If you want to pass arguments to the executable:

    edk r --foo --bar=XXX

## Debug the project

Similarly to the `r` subcommand, it's also possible to run the project with a debugger (only gdb for now):

    edk d

This will execute `gdb <executable>`.

## Clean build

    edk b -c

This will delete the build directory before performing a new build.
